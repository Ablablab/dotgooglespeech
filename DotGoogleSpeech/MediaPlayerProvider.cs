﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WMPLib;

namespace DotGoogleSpeech {
    public class MediaPlayerProvider {
        private static WindowsMediaPlayer instancePlayer { get; set; }
        private static WindowsMediaPlayer getPlayer() {
            if (instancePlayer == null) {
                instancePlayer = new WMPLib.WindowsMediaPlayer();
            }
            return instancePlayer;
        }
        public static void PlayFile(String path) {
            var Player = getPlayer();
            Player.PlayStateChange += Player_PlayStateChange;
            Player.URL = path;
            Player.controls.play();
        }
        public static void StopFile() {
            var Player = getPlayer();
            Player.controls.stop();
            Player.URL = null;
        }
        public static void CambiaVolume(int volume) {
            getPlayer().settings.volume = volume;
        }

        public static void Player_PlayStateChange(int NewState) {
            if ((WMPLib.WMPPlayState)NewState == WMPLib.WMPPlayState.wmppsStopped) {
                //Actions ons stop
            }
        }
        public static bool PlayerInCorso() {
            try {
                var stato = getPlayer().playState;
                if (stato == WMPPlayState.wmppsMediaEnded || stato == WMPPlayState.wmppsStopped) {
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (Exception e) {
                return true;
            }
            
        }
    }
}
