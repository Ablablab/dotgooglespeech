﻿namespace DotGoogleSpeech {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.button1 = new System.Windows.Forms.Button();
            this.textboxJsonApiKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCaricaJsonDaFile = new System.Windows.Forms.Button();
            this.selettoreFileJson = new System.Windows.Forms.OpenFileDialog();
            this.textboxInputText = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelFileJson = new System.Windows.Forms.Label();
            this.comboBoxIdVoce = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonStd = new System.Windows.Forms.RadioButton();
            this.radioButtonWaveNet = new System.Windows.Forms.RadioButton();
            this.trackBarVelocita = new System.Windows.Forms.TrackBar();
            this.trackBarTono = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioTesto = new System.Windows.Forms.RadioButton();
            this.radioSsml = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.comboSelettoreLingua = new System.Windows.Forms.ComboBox();
            this.buttonDialogSalvaFile = new System.Windows.Forms.Button();
            this.selettoreCartellaAudio = new System.Windows.Forms.FolderBrowserDialog();
            this.labelCartellaAudio = new System.Windows.Forms.Label();
            this.buttonAnteprima = new System.Windows.Forms.Button();
            this.labelVelocitaValore = new System.Windows.Forms.Label();
            this.labelTonoValore = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.trackBarVolumeAnteprima = new System.Windows.Forms.TrackBar();
            this.label7 = new System.Windows.Forms.Label();
            this.formatoOutputBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVelocita)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTono)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolumeAnteprima)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(283, 420);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Digitalizza audio";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.clickBottoneSintetizza);
            // 
            // textboxJsonApiKey
            // 
            this.textboxJsonApiKey.Location = new System.Drawing.Point(12, 29);
            this.textboxJsonApiKey.Multiline = true;
            this.textboxJsonApiKey.Name = "textboxJsonApiKey";
            this.textboxJsonApiKey.Size = new System.Drawing.Size(776, 29);
            this.textboxJsonApiKey.TabIndex = 1;
            this.textboxJsonApiKey.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Json API Key";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // buttonCaricaJsonDaFile
            // 
            this.buttonCaricaJsonDaFile.Location = new System.Drawing.Point(618, 3);
            this.buttonCaricaJsonDaFile.Name = "buttonCaricaJsonDaFile";
            this.buttonCaricaJsonDaFile.Size = new System.Drawing.Size(158, 22);
            this.buttonCaricaJsonDaFile.TabIndex = 3;
            this.buttonCaricaJsonDaFile.Text = "Carica Json da File";
            this.buttonCaricaJsonDaFile.UseVisualStyleBackColor = true;
            this.buttonCaricaJsonDaFile.Click += new System.EventHandler(this.buttonCaricaJsonDaFile_Click);
            // 
            // selettoreFileJson
            // 
            this.selettoreFileJson.FileName = "openFileDialog1";
            // 
            // textboxInputText
            // 
            this.textboxInputText.Location = new System.Drawing.Point(146, 60);
            this.textboxInputText.MaxLength = 132767;
            this.textboxInputText.Multiline = true;
            this.textboxInputText.Name = "textboxInputText";
            this.textboxInputText.Size = new System.Drawing.Size(573, 333);
            this.textboxInputText.TabIndex = 4;
            this.textboxInputText.Text = "Scrivere qui il testo da digitalizzare";
            this.textboxInputText.TextChanged += new System.EventHandler(this.textboxInputText_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.formatoOutputBox);
            this.panel1.Controls.Add(this.textboxInputText);
            this.panel1.Controls.Add(this.buttonCaricaJsonDaFile);
            this.panel1.Controls.Add(this.labelFileJson);
            this.panel1.Controls.Add(this.comboBoxIdVoce);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.trackBarVelocita);
            this.panel1.Controls.Add(this.trackBarTono);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.comboSelettoreLingua);
            this.panel1.Location = new System.Drawing.Point(12, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 410);
            this.panel1.TabIndex = 5;
            // 
            // labelFileJson
            // 
            this.labelFileJson.AutoSize = true;
            this.labelFileJson.Location = new System.Drawing.Point(94, 9);
            this.labelFileJson.Name = "labelFileJson";
            this.labelFileJson.Size = new System.Drawing.Size(0, 13);
            this.labelFileJson.TabIndex = 4;
            // 
            // comboBoxIdVoce
            // 
            this.comboBoxIdVoce.FormattingEnabled = true;
            this.comboBoxIdVoce.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D"});
            this.comboBoxIdVoce.Location = new System.Drawing.Point(3, 170);
            this.comboBoxIdVoce.Name = "comboBoxIdVoce";
            this.comboBoxIdVoce.Size = new System.Drawing.Size(135, 21);
            this.comboBoxIdVoce.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonStd);
            this.groupBox1.Controls.Add(this.radioButtonWaveNet);
            this.groupBox1.Location = new System.Drawing.Point(3, 196);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(135, 38);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // radioButtonStd
            // 
            this.radioButtonStd.AutoSize = true;
            this.radioButtonStd.Location = new System.Drawing.Point(67, 8);
            this.radioButtonStd.Name = "radioButtonStd";
            this.radioButtonStd.Size = new System.Drawing.Size(68, 17);
            this.radioButtonStd.TabIndex = 25;
            this.radioButtonStd.TabStop = true;
            this.radioButtonStd.Text = "Standard";
            this.radioButtonStd.UseVisualStyleBackColor = true;
            // 
            // radioButtonWaveNet
            // 
            this.radioButtonWaveNet.AutoSize = true;
            this.radioButtonWaveNet.Location = new System.Drawing.Point(0, 8);
            this.radioButtonWaveNet.Name = "radioButtonWaveNet";
            this.radioButtonWaveNet.Size = new System.Drawing.Size(71, 17);
            this.radioButtonWaveNet.TabIndex = 26;
            this.radioButtonWaveNet.TabStop = true;
            this.radioButtonWaveNet.Text = "WaveNet";
            this.radioButtonWaveNet.UseVisualStyleBackColor = true;
            // 
            // trackBarVelocita
            // 
            this.trackBarVelocita.Location = new System.Drawing.Point(3, 248);
            this.trackBarVelocita.Maximum = 400;
            this.trackBarVelocita.Minimum = 25;
            this.trackBarVelocita.Name = "trackBarVelocita";
            this.trackBarVelocita.Size = new System.Drawing.Size(135, 45);
            this.trackBarVelocita.TabIndex = 15;
            this.trackBarVelocita.Value = 25;
            this.trackBarVelocita.Scroll += new System.EventHandler(this.trackBarVelocita_Scroll);
            // 
            // trackBarTono
            // 
            this.trackBarTono.LargeChange = 1;
            this.trackBarTono.Location = new System.Drawing.Point(6, 291);
            this.trackBarTono.Maximum = 20;
            this.trackBarTono.Minimum = -20;
            this.trackBarTono.Name = "trackBarTono";
            this.trackBarTono.Size = new System.Drawing.Size(132, 45);
            this.trackBarTono.TabIndex = 16;
            this.trackBarTono.Scroll += new System.EventHandler(this.trackBarTono_Scroll);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Voce";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Velocità";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Lingua";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioTesto);
            this.groupBox2.Controls.Add(this.radioSsml);
            this.groupBox2.Location = new System.Drawing.Point(719, 170);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(70, 69);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            // 
            // radioTesto
            // 
            this.radioTesto.AutoSize = true;
            this.radioTesto.Location = new System.Drawing.Point(9, 16);
            this.radioTesto.Name = "radioTesto";
            this.radioTesto.Size = new System.Drawing.Size(52, 17);
            this.radioTesto.TabIndex = 15;
            this.radioTesto.TabStop = true;
            this.radioTesto.Text = "Testo";
            this.radioTesto.UseVisualStyleBackColor = true;
            // 
            // radioSsml
            // 
            this.radioSsml.AutoSize = true;
            this.radioSsml.Location = new System.Drawing.Point(9, 36);
            this.radioSsml.Name = "radioSsml";
            this.radioSsml.Size = new System.Drawing.Size(47, 17);
            this.radioSsml.TabIndex = 16;
            this.radioSsml.TabStop = true;
            this.radioSsml.Text = "Ssml";
            this.radioSsml.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 277);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Tono";
            // 
            // comboSelettoreLingua
            // 
            this.comboSelettoreLingua.FormattingEnabled = true;
            this.comboSelettoreLingua.Items.AddRange(new object[] {
            "en-US",
            "it-IT"});
            this.comboSelettoreLingua.Location = new System.Drawing.Point(3, 132);
            this.comboSelettoreLingua.Name = "comboSelettoreLingua";
            this.comboSelettoreLingua.Size = new System.Drawing.Size(135, 21);
            this.comboSelettoreLingua.TabIndex = 9;
            this.comboSelettoreLingua.SelectedIndexChanged += new System.EventHandler(this.comboSelettoreLinguaChanged);
            // 
            // buttonDialogSalvaFile
            // 
            this.buttonDialogSalvaFile.Location = new System.Drawing.Point(12, 420);
            this.buttonDialogSalvaFile.Name = "buttonDialogSalvaFile";
            this.buttonDialogSalvaFile.Size = new System.Drawing.Size(135, 23);
            this.buttonDialogSalvaFile.TabIndex = 6;
            this.buttonDialogSalvaFile.Text = "Scegli cartella file audio";
            this.buttonDialogSalvaFile.UseVisualStyleBackColor = true;
            this.buttonDialogSalvaFile.Click += new System.EventHandler(this.buttonDialogSalvaFile_Click);
            // 
            // labelCartellaAudio
            // 
            this.labelCartellaAudio.AutoSize = true;
            this.labelCartellaAudio.Location = new System.Drawing.Point(15, 404);
            this.labelCartellaAudio.Name = "labelCartellaAudio";
            this.labelCartellaAudio.Size = new System.Drawing.Size(0, 13);
            this.labelCartellaAudio.TabIndex = 7;
            // 
            // buttonAnteprima
            // 
            this.buttonAnteprima.Location = new System.Drawing.Point(465, 420);
            this.buttonAnteprima.Name = "buttonAnteprima";
            this.buttonAnteprima.Size = new System.Drawing.Size(91, 23);
            this.buttonAnteprima.TabIndex = 21;
            this.buttonAnteprima.Text = "Anteprima";
            this.buttonAnteprima.UseVisualStyleBackColor = true;
            this.buttonAnteprima.Click += new System.EventHandler(this.buttonAnteprima_Click);
            // 
            // labelVelocitaValore
            // 
            this.labelVelocitaValore.AutoSize = true;
            this.labelVelocitaValore.Location = new System.Drawing.Point(117, 313);
            this.labelVelocitaValore.Name = "labelVelocitaValore";
            this.labelVelocitaValore.Size = new System.Drawing.Size(0, 13);
            this.labelVelocitaValore.TabIndex = 27;
            this.labelVelocitaValore.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelTonoValore
            // 
            this.labelTonoValore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTonoValore.AutoSize = true;
            this.labelTonoValore.Location = new System.Drawing.Point(129, 356);
            this.labelTonoValore.Name = "labelTonoValore";
            this.labelTonoValore.Size = new System.Drawing.Size(0, 13);
            this.labelTonoValore.TabIndex = 28;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(728, 174);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 235);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 0;
            // 
            // trackBarVolumeAnteprima
            // 
            this.trackBarVolumeAnteprima.Location = new System.Drawing.Point(684, 420);
            this.trackBarVolumeAnteprima.Maximum = 100;
            this.trackBarVolumeAnteprima.Name = "trackBarVolumeAnteprima";
            this.trackBarVolumeAnteprima.Size = new System.Drawing.Size(104, 45);
            this.trackBarVolumeAnteprima.TabIndex = 29;
            this.trackBarVolumeAnteprima.Scroll += new System.EventHandler(this.trackBarVolumeAnteprima_Scroll);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(597, 425);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Volume anteprima";
            this.label7.Click += new System.EventHandler(this.label7_Click_1);
            // 
            // formatoOutputBox
            // 
            this.formatoOutputBox.FormattingEnabled = true;
            this.formatoOutputBox.Items.AddRange(new object[] {
            "MP3",
            "WAV"});
            this.formatoOutputBox.Location = new System.Drawing.Point(6, 372);
            this.formatoOutputBox.Name = "formatoOutputBox";
            this.formatoOutputBox.Size = new System.Drawing.Size(135, 21);
            this.formatoOutputBox.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 356);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Formato";
            this.label4.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.trackBarVolumeAnteprima);
            this.Controls.Add(this.labelTonoValore);
            this.Controls.Add(this.labelVelocitaValore);
            this.Controls.Add(this.buttonAnteprima);
            this.Controls.Add(this.buttonDialogSalvaFile);
            this.Controls.Add(this.labelCartellaAudio);
            this.Controls.Add(this.textboxJsonApiKey);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVelocita)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarTono)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVolumeAnteprima)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textboxJsonApiKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCaricaJsonDaFile;
        private System.Windows.Forms.OpenFileDialog selettoreFileJson;
        private System.Windows.Forms.TextBox textboxInputText;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonDialogSalvaFile;
        private System.Windows.Forms.FolderBrowserDialog selettoreCartellaAudio;
        private System.Windows.Forms.Label labelFileJson;
        private System.Windows.Forms.Label labelCartellaAudio;
        private System.Windows.Forms.ComboBox comboSelettoreLingua;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trackBarVelocita;
        private System.Windows.Forms.TrackBar trackBarTono;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxIdVoce;
        private System.Windows.Forms.Button buttonAnteprima;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelVelocitaValore;
        private System.Windows.Forms.Label labelTonoValore;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButtonStd;
        private System.Windows.Forms.RadioButton radioButtonWaveNet;
        private System.Windows.Forms.RadioButton radioTesto;
        private System.Windows.Forms.RadioButton radioSsml;
        private System.Windows.Forms.TrackBar trackBarVolumeAnteprima;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox formatoOutputBox;
    }
}