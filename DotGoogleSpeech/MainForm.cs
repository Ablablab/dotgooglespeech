﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DotGoogleSpeech {
    public partial class MainForm : Form {
        private bool isPlayingPreview;

        public MainForm() {
            InitializeComponent();
            impostaValoriApplicazione();
            caricaValoriPredefiniti();
            caricaImpostazioni();
        }
        private void impostaValoriApplicazione() {
            this.Text = "Digitalizzatore Google Cloud Text 2 Speech";
        }

        private void caricaImpostazioni() {
            try {
                selettoreFileJson.FileName = Properties.Settings.Default.FilePathJsonApi;
                labelFileJson.Text = Properties.Settings.Default.FilePathJsonApi;
                selettoreCartellaAudio.SelectedPath = Properties.Settings.Default.PathFileAudio;
                labelCartellaAudio.Text = Properties.Settings.Default.PathFileAudio;
                comboSelettoreLingua.Text = Properties.Settings.Default.Lingua;
                impostaTextOrSsml(Properties.Settings.Default.TextOrSsml);
                impostaTipoVoce(Properties.Settings.Default.TipoVoce);
                SetVelocita(Properties.Settings.Default.Velocita);
                ImpostaFormatoOutput(Properties.Settings.Default.FormatoOutput);
                ImpostaNomeVoce(Properties.Settings.Default.NomeVoce);
                SetTono(Properties.Settings.Default.Tono);
                SetVolumeAnteprima(Properties.Settings.Default.VolumeAnteprima);
                MediaPlayerProvider.CambiaVolume(trackBarVolumeAnteprima.Value);

                if (!String.IsNullOrEmpty(Properties.Settings.Default.FilePathJsonApi)) {
                    leggiJsonDaFile(Properties.Settings.Default.FilePathJsonApi);
                }
            }
            catch (Exception e) {
                //throw;
            }

        }



        private void salvaImpostazioni() {
            Properties.Settings.Default.FilePathJsonApi = GetJsonApiPath();
            Properties.Settings.Default.PathFileAudio = GetPathDestinazione();
            Properties.Settings.Default.Lingua = GetLingua();
            Properties.Settings.Default.TextOrSsml = (int)GetTextOrSsml();
            Properties.Settings.Default.TipoVoce = (int)GetTipoVoce();
            Properties.Settings.Default.NomeVoce = GetNomeVoce();
            Properties.Settings.Default.Velocita = GetVelocita();
            Properties.Settings.Default.FormatoOutput= (int)GetFormatoOutput();
            Properties.Settings.Default.Tono = GetTono();
            Properties.Settings.Default.VolumeAnteprima = GetVolumeAnteprima();
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Upgrade();
        }

        private void impostaTextOrSsml(int impostazione) {
            if (impostazione == (int)CodificaTesto.SSML) {
                this.radioTesto.Checked = false;
                this.radioSsml.Checked = true;
            }else { 
                this.radioTesto.Checked = true;
                this.radioSsml.Checked = false;
            }
             
        }
        private void impostaTipoVoce(int impostazione) {
            if (impostazione == (int)TipoVoce.Standard) {
                this.radioButtonWaveNet.Checked = false;
                this.radioButtonStd.Checked = true;
            }
            else if (impostazione == (int)TipoVoce.Wavenet) {
                this.radioButtonWaveNet.Checked = true;
                this.radioButtonStd.Checked = false;
            }
        }
        private void ImpostaFormatoOutput(int impostazione) {
            if (impostazione == (int)FormatoOutput.MP3) {
                this.formatoOutputBox.Text = FormatoOutput.MP3.ToString();
            }
            else if (impostazione == (int)FormatoOutput.WAV) {
                this.formatoOutputBox.Text = FormatoOutput.WAV.ToString();
            } else
            {
                this.formatoOutputBox.Text = FormatoOutput.MP3.ToString();
            }
        }

        private CodificaTesto GetTextOrSsml() {
            if (radioTesto.Checked) {
                return CodificaTesto.TEXT;
            }
            else {
                return CodificaTesto.SSML;
            }
        }
        private TipoVoce GetTipoVoce() {
            if (radioButtonWaveNet.Checked) {
                return TipoVoce.Wavenet;
            }
            else {
                return TipoVoce.Standard;
            }
        }

        public void caricaValoriPredefiniti() {
            if (String.IsNullOrEmpty(GetLingua())) {
                comboSelettoreLingua.SelectedIndex = 1;
            }
            if (radioSsml.Checked == radioTesto.Checked) {
                radioTesto.Checked = true;
                radioSsml.Checked = false;
            }
            if (radioButtonStd.Checked == radioButtonWaveNet.Checked) {
                radioButtonStd.Checked = false;
                radioButtonWaveNet.Checked = true;
            }  if (!String.IsNullOrEmpty(GetNomeVoce())) {
                ImpostaNomeVoce(GetNomeVoce());
            }
            trackBarTono.Value = 0;
            trackBarVelocita.Value = 100;
            trackBarVolumeAnteprima.Value = 5;

        }

        private void ImpostaNomeVoce(string v) {
            if (String.IsNullOrEmpty(v)) {
                return;
            }

            if (v.Length > 3) {
                comboBoxIdVoce.Text = v.Split('-').LastOrDefault();
            }
            else { 
            
            comboBoxIdVoce.Text = v;
            }
        }

        private string GetJsonApi() {
            return this.textboxJsonApiKey.Text;
        }
        private string GetJsonApiPath() {
            return this.selettoreFileJson.FileName;
        }
        private string GetInputText() {
            return this.textboxInputText.Text;
        }
        private string GetPathDestinazione() {
            return this.selettoreCartellaAudio.SelectedPath;
        }
        private string GetPathPreview() {
            return ".\\";
        }
        private string GetLingua() {
            return this.comboSelettoreLingua.Text;
        }

        private int GetTono() {
            return this.trackBarTono.Value;
        }
        private int GetVolumeAnteprima() {
            return this.trackBarVolumeAnteprima.Value;
        }
        private string GetNomeVoce() {
            //it-IT-Wavenet-A
            return $"{GetLingua()}-{GetTipoVoce().ToString()}-{this.comboBoxIdVoce.Text}";
        }
        private double GetVelocita() {
            return this.trackBarVelocita.Value * 1.0 / 100;
        }
        private FormatoOutput GetFormatoOutput() {
            var valoreInGrafica = this.formatoOutputBox.Text;
            if (FormatoOutput.MP3.ToString().Equals(valoreInGrafica, StringComparison.OrdinalIgnoreCase)) {
                return FormatoOutput.MP3;
            }
            else if (FormatoOutput.WAV.ToString().Equals(valoreInGrafica, StringComparison.OrdinalIgnoreCase)) {
                return FormatoOutput.WAV;
            }
            else {
                return FormatoOutput.MP3;
            }
        }
        private void SetTono(int valore) {
            this.trackBarTono.Value = valore;
        }
        private void SetVolumeAnteprima(int valore) {
            this.trackBarVolumeAnteprima.Value = valore;
        }
        private void SetVelocita(double valore) {
            if (valore != 0) {

                this.trackBarVelocita.Value = Convert.ToInt32(valore * 100.0);
            }
        }
 

        private bool campiNecessariPerSintetizzatoreCompilati() {
            if (String.IsNullOrEmpty(GetJsonApi())) {
                MessageBox.Show("Indicare il file json con le credenziali");
                return false;
            }
            if (String.IsNullOrEmpty(GetInputText())) {
                MessageBox.Show("Il testo da sintetizzare non può essere vuoto");
                return false;
            }
            if (String.IsNullOrEmpty(GetNomeVoce())) {
                MessageBox.Show("Indicare una voce");
                return false;
            }
            if (String.IsNullOrEmpty(GetLingua())) {
                MessageBox.Show("Indicare una lingua");
                return false;
            }
            if (String.IsNullOrEmpty(GetNomeVoce())) {
                MessageBox.Show("Indicare una voce");
                return false;
            }
            return true;
        }

        private void clickBottoneSintetizza(object sender, EventArgs e) {
            // ascolta, problemi su salva in cartella utente,  nomi voce,
            this.UseWaitCursor = true;
            try {
                if (campiNecessariPerSintetizzatoreCompilati()) {
                    var jsonApi = GetJsonApi();
                    var inputText = GetInputText();
                    var textOrSsml = GetTextOrSsml();
                    var pathDestinazione = GetPathDestinazione();
                    var lingua = GetLingua();
                    var nomeVoce = GetNomeVoce();
                    var velocita = GetVelocita();
                    var formatoOutput = GetFormatoOutput();
                    var tono = GetTono();
                    var asyncTask = Task.Run(() => GCloudProvider.RichiediSintetizzazioneViaAPI(jsonApi, null,inputText, textOrSsml, pathDestinazione, lingua,  nomeVoce, velocita, tono, formatoOutput));
                    asyncTask.ContinueWith((t) => {
                        if (!t.IsCanceled && !t.IsFaulted && t.IsCompleted) {
                            var pathNuovoFile = t.Result;
                            MessageBox.Show($"Generato nuovo file {pathNuovoFile}");
                        }
                        else {
                            MessageBox.Show(t.Exception?.ToString() + t.Exception?.Message, "Errore");
                        }
                    });

                    salvaImpostazioni();
                }

            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString() + ex.Message, "Errore");
            }
            finally {
                this.UseWaitCursor = false;
            }
        }




        private void label1_Click(object sender, EventArgs e) {

        }

        private void textBox1_TextChanged(object sender, EventArgs e) {

        }

        private void buttonCaricaJsonDaFile_Click(object sender, EventArgs e) {
            if (selettoreFileJson.ShowDialog() == DialogResult.OK) {
                try {
                    labelFileJson.Text = "";
                    var filePath = selettoreFileJson.FileName;
                    Properties.Settings.Default.FilePathJsonApi = filePath;


                    leggiJsonDaFile(filePath);
                    labelFileJson.Text = filePath;

                }
                catch (SecurityException ex) {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void leggiJsonDaFile(string filePath) {
            using (var str = new StreamReader(File.OpenRead(filePath))) {
                this.textboxJsonApiKey.Text = str.ReadToEnd();
            }
        }

        private void buttonDialogSalvaFile_Click(object sender, EventArgs e) {
            if (selettoreCartellaAudio.ShowDialog() == DialogResult.OK) {
                try {
                    labelCartellaAudio.Text = "";


                    labelCartellaAudio.Text = selettoreCartellaAudio.SelectedPath;
                }
                catch (SecurityException ex) {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }



        private void comboSelettoreLinguaChanged(object sender, EventArgs e) {

        }

        private void label4_Click(object sender, EventArgs e) {

        }

        private void MainForm_Load(object sender, EventArgs e) {

        }

        private void radioTesto_CheckedChanged(object sender, EventArgs e) {
            //radioSsml.Checked = !radioSsml.Checked;
        }

        private void radioSsml_CheckedChanged(object sender, EventArgs e) {
            //radioTesto.Checked = !radioTesto.Checked;
        }

        private void label7_Click(object sender, EventArgs e) {

        }

        private void trackBarTono_Scroll(object sender, EventArgs e) {
            labelTonoValore.Text = GetTono().ToString();
        }

        private void trackBarVelocita_Scroll(object sender, EventArgs e) {
            labelVelocitaValore.Text = GetVelocita().ToString();
        }

        private void radioButtonStd_CheckedChanged(object sender, EventArgs e) {

        }

        private void button2_Click(object sender, EventArgs e) {

        }

        private void buttonAnteprima_Click(object sender, EventArgs e) {
            if (isPlayingPreview) {
                StopPreview();
            }
            else {
                // ascolta, problemi su salva in cartella utente,  nomi voce,
                this.UseWaitCursor = true;
                try {
                    if (campiNecessariPerSintetizzatoreCompilati()) {
                        var jsonApi = GetJsonApi();
                        var inputText = GetInputText();
                        var textOrSsml = GetTextOrSsml();
                        var pathDestinazione = GetPathPreview();
                        var nomePreview = GetNomePreview();
                        var lingua = GetLingua();
                        var formatoOutput = GetFormatoOutput();
                        var nomeVoce = GetNomeVoce();
                        var velocita = GetVelocita();
                        var tono = GetTono();
                        
                        var asyncTask = Task.Run(() => GCloudProvider.RichiediSintetizzazioneViaAPI(jsonApi,nomePreview, inputText, textOrSsml, pathDestinazione, lingua,  nomeVoce, velocita, tono,formatoOutput));
                        
                        asyncTask.ContinueWith((t) => {
                            if (!t.IsCanceled && !t.IsFaulted && t.IsCompleted) {
                                //MessageBox.Show($"Generato nuovo file {pathNuovoFile}");

                            }
                            else {
                                MessageBox.Show(t.Exception?.ToString() + t.Exception?.Message, "Errore");
                                
                            }
                        });
                        asyncTask.Wait();
                        if (asyncTask.IsCompleted && !asyncTask.IsCanceled && !asyncTask.IsFaulted) { 
                            StartPreview(asyncTask.Result);
                        
                        }

                    }

                }
                catch (Exception ex) {
                    MessageBox.Show(ex.ToString() + ex.Message, "Errore");
                }
                finally {
                    this.UseWaitCursor = false;
                }

                
            }
        }

        private string GetNomePreview() {
            return "preview";
        }

        private void StartPreview(string path) {
            this.isPlayingPreview = true;
            this.buttonAnteprima.Text = "Stop";
            MediaPlayerProvider.PlayFile(path);
            Task.Run(() => ControllaPreviewFinita());
            
        }
        private void ControllaPreviewFinita() {
            if (MediaPlayerProvider.PlayerInCorso()) {
                Thread.Sleep(300);
                ControllaPreviewFinita();
            }
            else {
                this.Invoke((System.Action)(() => {
                    //your thread call or definition
                    StopPreview();
                }));
                
            }
        }

        private void StopPreview() {
            this.isPlayingPreview = false;
            this.buttonAnteprima.Text = "Anteprima";
            MediaPlayerProvider.StopFile();
        }

        private void radioButtonWaveNet_CheckedChanged(object sender, EventArgs e) {

        }

        private void label7_Click_1(object sender, EventArgs e) {

        }

        private void trackBarVolumeAnteprima_Scroll(object sender, EventArgs e) {
            MediaPlayerProvider.CambiaVolume(trackBarVolumeAnteprima.Value);
        }

        private void textboxInputText_TextChanged(object sender, EventArgs e) {

        }

        private void label4_Click_1(object sender, EventArgs e) {

        }
    }
}
