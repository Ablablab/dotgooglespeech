﻿using Google.Cloud.TextToSpeech.V1;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGoogleSpeech {
    public class GCloudProvider {
        public static string RichiediSintetizzazioneViaAPI(string jsonCredential, string forzaNomeFile, string inputText, CodificaTesto textOrSsml, string pathDestinazione, string lingua, string nomeVoce, double speakingRate, double pitch, FormatoOutput formatoOutput) {
            // Instantiate a client
            var builder = new TextToSpeechClientBuilder();
            builder.JsonCredentials = jsonCredential;

            TextToSpeechClient client = builder.Build();

            // Set the text input to be synthesized.
            SynthesisInput input = null;
            if (textOrSsml == CodificaTesto.TEXT) {
                input = new SynthesisInput {
                    Text = inputText,
                };
            }
            else if (textOrSsml == CodificaTesto.SSML) {
                input = new SynthesisInput {
                    Ssml = inputText,
                };
            }


            // Build the voice request, select the language code ("en-US"),
            // and the SSML voice gender ("neutral").
            VoiceSelectionParams voice = new VoiceSelectionParams {
                LanguageCode = lingua,
                Name = nomeVoce
            };


            // Select the type of audio file you want returned.
            AudioConfig config = new AudioConfig {
                AudioEncoding = daFormatoOutputAAudioEncoding(formatoOutput),
                SpeakingRate = speakingRate,
                Pitch = pitch
            };

            SynthesizeSpeechResponse response;
            // Perform the Text-to-Speech request, passing the text input
            // with the selected voice parameters and audio file type
            try {
                response = client.SynthesizeSpeech(new SynthesizeSpeechRequest {
                    Input = input,
                    Voice = voice,
                    AudioConfig = config
                });
            }
            catch (RpcException e) {
                throw new Exception(e.Status.Detail);
            }

            // Write the binary AudioContent of the response to an MP3 file.
            if (!pathDestinazione.EndsWith("\\")) {
                pathDestinazione = pathDestinazione + "\\";
            }
            var pathNuovoFile = pathDestinazione + GeneraNomeFile(inputText, forzaNomeFile, formatoOutput.ToString());
            using (Stream output = File.Create(pathNuovoFile)) {
                response.AudioContent.WriteTo(output);

            }
            return pathNuovoFile;
        }


        public static AudioEncoding daFormatoOutputAAudioEncoding(FormatoOutput formatoOutput) {
            switch (formatoOutput) {
                case FormatoOutput.MP3:
                    return AudioEncoding.Mp3;
                case FormatoOutput.WAV:
                    return AudioEncoding.Linear16;
                default:
                    return AudioEncoding.Mp3;
            }
        }


        public static string GeneraNomeFile(string input, string forzaNomeFile, string estensione) {
            if (!String.IsNullOrEmpty(forzaNomeFile)) {
                return $"{forzaNomeFile}.{estensione}";
            }
            else {

                input = new String(input.Where(x => Char.IsDigit(x) || Char.IsLetter(x)).ToArray());
                if (input.Length > 50) {
                    input = input.Substring(0, 40) + "__" + input.Substring(input.Count() - 10, 10);
                }
                return $"{input}.{estensione}";
            }
        }

    }
}
