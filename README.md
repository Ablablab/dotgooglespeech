# Simple Windows GUI Client to Invoke Google Cloud API for Text-To-Speech
## Description
This is a simple (very simple) client GUI developed on WinForm to inovoke Google Cloud Text-To-Speech API. 
Now is totally in italian, but if you need you can translate it and add a pull-request (or you can ask me to translate it, if I can).
You can use it to try and play with this incredible Google Cloud API. 
Feel free to open new Issues if you need help or modifications. 

## Google Cloud Text-To-Speech
### Text-to-speech conversion powered by machine learning.

Go to Official Google Cloud TTS -> [Docs](https://cloud.google.com/text-to-speech)
## Screenshot
![Screenshot](./screenshot.png)

## Prerequisites

* Account on [Google Cloud](https://cloud.google.com/)
* Json API Key for Google Cloud TTS (Follow [the official guide](https://cloud.google.com/text-to-speech/docs/quickstart-client-libraries))
* Windows 

## Installation
* Download Latest ZipPackage on [ReleaseExe](./ReleaseExe)
* Unzip the file
* Execute setup.exe
* Enjoy it!

## Build 

Requires .Net Framework 4.6.1

* Clone this repository
* Add file DotGoogleSpeech/DotGoogleSpeech.csproj as existing project in Visual Studio 2019+
* You are ready!
